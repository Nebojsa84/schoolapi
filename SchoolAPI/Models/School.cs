﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolAPI.Models
{
    public class School
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int Year { get; set; }

        public School(int id, string name, string city, int year)
        {
            Id = id;
            Name = name;
            City = city;
            Year = year;
        }
    }
}