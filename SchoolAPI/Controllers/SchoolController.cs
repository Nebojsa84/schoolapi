﻿using SchoolAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolAPI.Controllers
{
    public class SchoolController : ApiController
    {
        private List<School> schools = new List<School>()
        {
            new School(1,"School1","City1",2001),
            new School(2,"School2","City2",2005),
            new School(3,"School3","City3",2010),
            new School(4,"School4","City4",1999)
        };

        public IHttpActionResult Get()
        {
            return Ok(schools);
        }

        public IHttpActionResult Get(int id)
        {
            var school = schools.FirstOrDefault(s => s.Id == id);
            if (school == null)
            {
                return NotFound();
            }

            return Ok(school);
        }

        public IHttpActionResult Post(School school)
        {
            var check = schools.FirstOrDefault(s => s.Id == school.Id);
            if (check == null)
            {
                schools.Add(school);
                return Ok(schools);
            }
            return BadRequest();
        }

        public IHttpActionResult Put(School school)
        {

            var check = schools.FirstOrDefault(s => s.Id == school.Id);
            if (check == null)
            {
                return NotFound();
            }

            check.City = school.City;
            check.Name = school.Name;
            check.Year = school.Year;
            return Ok(schools);
        }


        public IHttpActionResult Delete(int id)
        {
            var school = schools.FirstOrDefault(s => s.Id == id);
            if (school == null)
            {
                return NotFound();
            }

            schools.Remove(school);
            return Ok(schools);
        }
        [Route("api/school/ByYear")]
        public IHttpActionResult GetByYear([FromBody]int Year)
        {
            var retVal = schools.Where(s => s.Year > Year);
            if (retVal == null)
            {
                return NotFound();
            }

            return Ok(retVal);
        }

    }
}
